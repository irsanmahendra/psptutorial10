package com.example;

import static org.junit.Assert.assertNotNull;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import com.example.model.StudentModel;
import com.example.service.StudentService;

import junit.framework.Assert;

@RunWith(SpringRunner.class)
@SpringBootTest
@Transactional
public class StudentServiceTest {

	@Autowired
	StudentService service;

	@Test
	public void testSelectAllStudents() {
		List<StudentModel> students = service.selectAllStudents();
		Assert.assertNotNull("Gagal = student menghasikan null", students);
		Assert.assertEquals("Gagal - size student tidak sesuai", 5, students.size());
	}

	@Test
	public void testSelectStudent() {
		StudentModel student = service.selectStudent("1506797362");
		Assert.assertNotNull("Gagal - student menghasilkan null", student);
		Assert.assertEquals("Gagal - nama student tidak sesuai dengan di database", "irsan mahendra",
				student.getName());
		Assert.assertEquals("Gagal - GPA student tidak sesuai dengan di database", 4.0, student.getGpa(), 0.0);
	}

	@Test
	public void testCreateStudent() {
		StudentModel student = new StudentModel("126", "Budi", 3.44, null);
		// Cek apakah student sudah ada
		Assert.assertNull("Mahasiswa sudah ada", service.selectStudent(student.getNpm()));
		// Masukkan ke service
		service.addStudent(student);
		// Cek apakah student berhasil dimasukkan
		Assert.assertNotNull("Mahasiswa gagal dimasukkan", service.selectStudent(student.getNpm()));
	}
	
	@Test
	public void testUpdateStudent() {
		String npm = "1506797362";
		StudentModel student = service.selectStudent(npm);
		
		student.setName("Irsan");
		student.setGpa(3.9);
		service.updateStudent(student);
		
		student = service.selectStudent(npm);
		Assert.assertEquals("Nama mahasiswa tidak sesuai dengan data yang di update", "Irsan", student.getName());
		Assert.assertEquals("GPA mahasiswa tidak sesuai dengan data yang di update", 3.9, student.getGpa(), 0.0);
		
	}
	
	@Test
	public void testDeleteStudent() {
		String npm = "1506797362";
		StudentModel student = service.selectStudent(npm);
		Assert.assertNotNull("Mahasiswa yang diambil tidak ditemukan", student);
		service.deleteStudent(npm);
		
		student = service.selectStudent(npm);
		Assert.assertNull("Mahasiswa masih ada di database", student);
		
	}
}
